# Target blank vulnerability

tl;dr Any website you open in a new tab from an anchor with attribute `target="_blank"` can access the tab from which the anchor was clicked and modify its contents.

## Don't do this
`<a href="Page2.html" target="_blank">Page 2</a>`

## Do this
`<a href="Page2.html" rel="noopener noreferrer" target="_blank">Page 2</a>`

## Learn why
[Target="_blank" - the most underestimated vulnerability ever](https://www.jitbit.com/alexblog/256-targetblank---the-most-underestimated-vulnerability-ever/)
